# Assignment: Sepia filter in C and Assembly

Лабораторная работа: фильтр сепия на C и Ассемблере

# Вывод программы
```
make: Entering directory '/home/vladimir/Projects/Alina/assignment-5-sepia-filter'
```
```
rm -rf build
mkdir -p build
gcc -c -g -O0 -Wall -Werror -std=c17 -Wdiscarded-qualifiers -Wincompatible-pointer-types -Wint-conversion -o build/bmp.o src/bmp.c
mkdir -p build
gcc -c -g -O0 -Wall -Werror -std=c17 -Wdiscarded-qualifiers -Wincompatible-pointer-types -Wint-conversion -o build/image.o src/image.c
mkdir -p build
gcc -c -g -O0 -Wall -Werror -std=c17 -Wdiscarded-qualifiers -Wincompatible-pointer-types -Wint-conversion -o build/sepia_filter_c.o src/sepia_filter_c.c
mkdir -p build
gcc -c -g -O0 -Wall -Werror -std=c17 -Wdiscarded-qualifiers -Wincompatible-pointer-types -Wint-conversion -o build/main.o src/main.c
mkdir -p build
nasm -felf64 -o build/sepia_filter_asm.o src/sepia_filter_asm.asm
gcc -no-pie -o build/main build/bmp.o build/image.o build/sepia_filter_c.o build/main.o build/sepia_filter_asm.o
```
## C:
```
File has been loaded
Width: 1920
Height: 1080
Process has been finished. Time taken: 66592μs
Image has been saved
```
## NASM:
```
File has been loaded
Width: 1920
Height: 1080
Process has been finished. Time taken: 5184μs
Image has been saved
```
```
make: Leaving directory '/home/vladimir/Projects/Alina/assignment-5-sepia-filter'
```

# Задание

В данном задании вам надо реализовать фильтр сепия на языке C и на языке ассемблера. 
- Реализация на языке ассемблера должна использовать векторные инструкции процессора SSE, за счет чего должно быть получено ускорение по сравнению с реализацией на C. 
- Задание считается сданным только в случае ускорения работы фильтра.
- Необходимо написать тест, который будет сравнивать время работы двух реализаций.
- Для работы с изображениями следует использовать библиотеку работы с BMP файлами, которую вы реализовали в третьей лаботаторной работе.
