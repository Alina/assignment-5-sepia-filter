#include "../include/bmp.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#define HEADER_TYPE 19778
#define HEADER_RESERVED 0
#define HEADER_OFFBITS 54
#define HEADER_SIZE  40
#define HEADER_PLANES 1
#define HEADER_BIT_COUNT 24
#define HEADER_COMPRESSION 0
#define HEADER_PIXELS_PER_METER 2834
#define HEADER_COLORS_USED 0
#define HEADER_COLORS_IMPORTANT 0

static inline size_t get_padding_size(size_t width) {
    return (4 - (3 * width % 4)) % 4;
}

enum read_status from_bmp(FILE* in, struct image* img) {
    struct bmpHeader header = {0};
    size_t count = fread(&header, sizeof(struct bmpHeader), 1, in);

    if (count != 1) return READ_INVALID_HEADER;

    if (header.bfType != HEADER_TYPE) return READ_INVALID_SIGNATURE;

    if (header.biBitCount != HEADER_BIT_COUNT) return READ_INVALID_BIT_COUNT;

    *img = create_image(header.biWidth, header.biHeight);
    size_t padding_size = get_padding_size(header.biWidth);

    fseek(in, header.bOffBits, SEEK_SET);

    for (size_t _y = img->height; _y > 0; _y--) {
        size_t y = _y - 1;

        size_t count = fread(img->data + y * img->width, sizeof(struct pixel), img->width, in);
        if (count < img->width) {
            return READ_INVALID_BITS;
        }

        for (size_t x = 0; x < img->width; x++) {
            struct pixel p = get_pixel(img, x, y);
            uint8_t temp = p.r;
            p.r = p.b;
            p.b = temp;
            set_pixel(img, x, y, p);
        }

        if (fseek(in, padding_size, SEEK_CUR) != 0) {
            return READ_INVALID_BITS;
        }
    }

    return READ_OK;
}

enum write_status to_bmp(FILE* out, const struct image* img) {
    uint64_t padding_size = get_padding_size(img->width);
    uint64_t lineSize = sizeof(uint8_t) * (img->width * 3 + padding_size);

    struct bmpHeader header = {0};
    header.bfType = HEADER_TYPE;
    header.biSizeImage = (img->width*3 + padding_size) * img->height;
    header.bfileSize = header.biSizeImage + sizeof(struct bmpHeader);
    header.bfReserved = HEADER_RESERVED;
    header.bOffBits = HEADER_OFFBITS;
    header.biSize = HEADER_SIZE;
    header.biWidth = img->width;
    header.biHeight = img->height;
    header.biPlanes = HEADER_PLANES;
    header.biBitCount = HEADER_BIT_COUNT;
    header.biCompression = HEADER_COMPRESSION;

    header.biXPelsPerMeter = HEADER_PIXELS_PER_METER;
    header.biYPelsPerMeter = HEADER_PIXELS_PER_METER;
    header.biClrUsed = HEADER_COLORS_USED;
    header.biClrImportant = HEADER_COLORS_IMPORTANT;

    size_t count = fwrite(&header, sizeof(struct bmpHeader), 1, out);

    if (count < 1) {
        return WRITE_ERROR;
    }

    for (size_t _y = img->height; _y > 0; _y--) {
        size_t y = _y - 1;

        uint8_t* scanLine = malloc(lineSize);

        for (size_t x = 0; x < img->width; x++) {
            struct pixel pixel = get_pixel(img, x, y);
            scanLine[3*x] = pixel.b;
            scanLine[3*x+1] = pixel.g;
            scanLine[3*x+2] = pixel.r;
        }

        for (size_t x = 0; x < padding_size; x++) {
            scanLine[img->width*3 + x] = 0;
        }

        size_t count = fwrite(scanLine, sizeof(uint8_t), img->width*3+padding_size, out);
        free(scanLine);

        if (count < img->width + padding_size) {
            return WRITE_ERROR;
        }

        if (y == 0) break;
    }

    return WRITE_OK;
}
