#include "../include/bmp.h"
#include "../include/image.h"
#include "../include/sepia_filter_c.h"
#include <inttypes.h>
#include <stdio.h>
#include <sys/resource.h>

int main(int argc, char** argv) {
    if (argc < 4) {
        puts("Not enough arguments");
        puts("Correct syntax: sepia <input_path> <output_path> <c|a>");
        return 1;
    }
    FILE* in = fopen(argv[1], "rb");

    if (in == NULL) {
        printf("Failed to open source file\n");
        return 1;
    }

    struct image img = {0};
    enum read_status rs = from_bmp(in, &img);

    switch (rs) {
        case READ_OK:
            printf("File has been loaded\n");
            printf("Width: %" PRIu64 "\nHeight: %" PRIu64 "\n", img.width, img.height);
            break;
        case READ_INVALID_SIGNATURE:
            printf("Failed to load image: Invalid signature\n");
            return 1;
        case READ_INVALID_HEADER:
            printf("Failed to load image: Invalid header\n");
            return 1;
        case READ_INVALID_BITS:
            printf("Failed to load image: Invalid bits\n");
            return 1;
        case READ_INVALID_BIT_COUNT:
            printf("Failed to load image: Bit count not supported\n");
            return 1;
    }

    if (fclose(in) != 0) {
        printf("Failed to close source file\n");
        return 1;
    }

    char mode = argv[3][0];
    struct image result = {0};

    struct rusage r = {0};
    getrusage(RUSAGE_SELF, &r);
    const long start = r.ru_utime.tv_usec;

    switch (mode) {
        case 'c':
            result = sepia_c(&img);
            break;
        case 'a':
            result = sepia_asm(&img);
            break;
        default:
            puts("Invalid sepia, use c|a");
            destroy_image(&img);
            return 1;
    }

    getrusage(RUSAGE_SELF, &r);
    printf("Process has been finished. Time taken: %ldμs\n", r.ru_utime.tv_usec - start);

    FILE* out = fopen(argv[2], "wb");

    if (out == NULL) {
        printf("Failed to open target file\n");
        return 1;
    }

    enum write_status ws = to_bmp(out, &result);

    switch (ws) {
        case WRITE_OK:
            printf("Image has been saved\n");
            break;
        case WRITE_ERROR:
            printf("Failed to save image\n");
            break;
    }

    destroy_image(&result);

    if (fclose(out) != 0) {
        printf("Failed to close target file\n");
        return 1;
    }

    return 0;
}
